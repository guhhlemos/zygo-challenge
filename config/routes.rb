Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'drinks#index'


  get '/recommendation', to: 'drinks#recommendation'

  resources :home, only: %i[index]
end
