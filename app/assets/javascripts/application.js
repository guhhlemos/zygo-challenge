// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .
// 
//= require jquery3
//= require popper
//= require bootstrap

function ready(event) {
  $('input[type=radio][name=searchRadio]').change(function() {
    if (this.value === 'simple') {
      $('.simple-form').show()
      $('.advanced-form').hide()
    }
    else if (this.value === 'advanced') {
      $('.simple-form').hide()
      $('.advanced-form').show()
    }
  })

  $('.give_anything').on('click', function () {
    $(this).closest('form').append($('<input>', {
      name: "give_anything",
      type: "hidden",
      value: true
    })).submit()
  })

  $(document).on('click', '.page-link', function (e) {
    e.preventDefault()

    $.ajax({
      url: $(this).attr('href'),
      // data: $(this).serialize(),
      success: function (data) {
        // console.log(data)
        $('.drinks-list').html(data);
      },
      complete: function (data) {
        // console.log(data.responseText)
      },
      error: function (data) {
        alert('ajax error')
        // console.log(data)
      },
    })
  })

  $('form').on('submit', function (e) {

    e.preventDefault()

    // console.log($(this).attr('action'))
    // console.log($(this).serialize())

    $.ajax({
      url: $(this).attr('action'),
      data: $(this).serialize(),
      type: $(this).attr('method'),
      success: function (data) {
        // console.log(data)
        $('.drinks-list').html(data);
      },
      complete: function (data) {
        // console.log(data.responseText)
        $('[name="give_anything"]').remove()
      },
      error: function (data) {
        alert('ajax error')
        // console.log(data)
      },
    })
  })
}

$(document).on('turbolinks:load', ready)
