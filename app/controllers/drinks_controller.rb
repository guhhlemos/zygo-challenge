class DrinksController < ApplicationController
  def index
    if params[:search]
      @drinks = Drink.paginate(page: params[:page], per_page: 1).where('name ILIKE ?', "%#{params[:search]}%").order(:created_at)
      render :partial => 'drinks_list'
    else
      @drinks = Drink.paginate(page: params[:page], per_page: 1).order(:created_at)
      if params[:page]
        render :partial => 'drinks_list'
      end
    end
  end

  def recommendation
    # _origin = params[:origin].capitalize
    # _origin.gsub!('_', ' ')

    if params[:give_anything]
      @drinks = Drink
      .limit(1)
      .order("RANDOM()") # POSTGRESQL
      # .order("RAND()") # MYSQL


      render :partial =>  'recommendation'

    else

      _avg_alcohol_level = 30

      _condition = params[:get_drunk] == 'yes' ? '>' : '<'

      _partial_drinks = Drink.paginate(page: params[:page], per_page: 100)

      _partial_drinks = _partial_drinks.where("alcohol_level #{_condition} ?", "#{_avg_alcohol_level}")

      if  ! ['No', 'Other'].include? params[:origin]
        _partial_drinks = _partial_drinks.where('origin = ?', "#{params[:origin]}")
      end

      _partial_drinks = _partial_drinks.order(:created_at)

      @drinks = _partial_drinks

      # if params[:get_drunk] == 'yes'
      #   @message = 'Since you wanna to get crazy as hell, we recommend a drink with alcohol level more than 30%'
      # end

      _messages = []
      #
      # _messages.push('msg1')
      # _messages.push('msg2')
      # _messages.push('msg3')
      #
      # @messages = _messages
      # @message = _messages.join(". ")

      params[:get_drunk] == 'yes' ?
        _messages.push("Since you wanna to go crazy as hell we recommend a drink with alcohol level higher than 30%") :
        _messages.push("Alright, you don't want to get drunk, so we recommend a drink with alcohol level below 30% (like you would not be drunk)")

      if params[:russian]
        _messages.push("And if you're really Russian then you deserve Vodka!")
      end

      @messages = _messages

      # @drinks = Drink
      #   .paginate(page: params[:page], per_page: 100)
      #   .where("alcohol_level #{_condition} ?", "#{_avg_alcohol_level}")
      #   .order(:created_at)


      render :partial =>  'recommendation'
    end




    # @drinks = Drink
    #   .paginate(page: params[:page], per_page: 100)
    #   .where('origin = ?', "#{params[:origin]}")
    #   .order(:created_at)

    # render plain: _origin.inspect
    # render :partial =>  'recommendation'
  end
end
